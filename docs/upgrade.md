# Upgradera Grav och/eller PHP

## Uppgradera image

Detta exempel bygger på en förenkling där man inte jobbar med en branch innan ändringar.
Det passar bäst med mindre ändringar där inte flera personer jobbar med samma kod
amtidigt.

* Försäkra dig att du har senaste main-branchen. Kommando: `git pull`

* Öppna filen `Dockerfile`, följande två ställen styr vilka versioner av PHP och Grav som körs.
  * Första raden pekar på vilken PHP-version som används. Efter kolonet(:) står versionsnummer.
    Vilka versioner som finns tillgängliga finns på [Docker Hub](https://hub.docker.com/_/php?tab=tags&page=1&ordering=last_updated)
    Det är viktigt att vi kör versioner med `-apache` då imagen är beroende av Apache.

  * Tredje raden sätter miljövariablen `GRAV_VER` som talar om versionen av Grav som skall användas.
    Aktuell version att användas finns listat på [Gravs hemsida](https://getgrav.org/).


* Commita ändringen till branchen. Kommando: `git commit -a -m "Upgrade Grav and PHP"`

* Pusha till GitLab. Kommando: `git push`

* Nu kommer förändringen att byggas och automatiskt deploya på en utvecklingsmiljö.

* När bygget är klart syns det på [repositoryts hemsida](https://gitlab.com/arbetsformedlingen/www/jobtechdev-se/-/tree/main) där en ruta beskriver den sista commiten och har till höger en ring med en bock. Om ett kryss visas har det gått fel.

![Bygget är klart](images/build-done.png)

* Just nu finns inget enkelt sätt att se om ändringen blivit installerad men det skall i framtiden synas på samma sätt som bygget, fast i [infrastruktur repositoryt](https://gitlab.com/arbetsformedlingen/www/jobtechdev-se-infra/-/tree/main). Installationen skall ta max 5 minuter.

* När både bygget och installationen är färdig syns resultatet [här](jobtechdev-se-jobtechdev-se-develop.test.services.jtech.se). Detta är helt separerat från produktion och vår officiella hemsida. Verifiera att din förändring är korrekt och att allt fungerar som det ska. Tänk på att din webbläsare kan ha cach:at, så ladda om sidan med Ctrl-R om det inte slår igenom.

## Driftsätt

* När du är nöjd med ändringen är det dags att installera i produktion. Detta styrs via GitLab.

* Gå till [kod-repositoryt avdelning för taggar](https://gitlab.com/arbetsformedlingen/www/jobtechdev-se/-/tags).

* Du ser att det finns en tag som heter `prod`, den pekar på vad som är i produktion just nu. Ta bort den genom att trycka på papperskorgen. Bekräfta att du vill raddera den.

* Klicka på *New tag* för att skapa en ny tag.

* Fyll `prod` i fälltet *Tag name*

* Klicka på *Create tag*. Nu blir den senaste ändringen markerad för produktion. Den kommer att hämtas och installeras av produktionsmiljön inom 10 minuter.

* I dagsläget finns inget enkelt sätt att se om ändringen har installerats på produktionsmiljön.
Ladda hemsidan för att kontrollera utseendet och ändringarna har fastnat. Tänk på att din webbläsare kan ha cach:at, så ladda om sidan med Ctrl-R om det inte slår igenom.

## Bra att veta!

* Varje gång en ny release sker så uppgraderas pluginen till Grav automatiskt. Detta sker även när exvis också
  temat har ändrats.
