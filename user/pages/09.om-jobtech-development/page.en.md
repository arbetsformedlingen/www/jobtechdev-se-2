---
title: 'About JobTech Development'
custom:
    content: "### From ego to eco.\n## The labor market is changing faster than ever and offers hundreds of digital guidance and matching services. In order for people to be able to navigate and find each other, the services need to communicate and adapt to new needs. It requires a new form of digital infrastructure.\n\nJobTech Development is Arbetsförmedlingen's investment in a sustainable and common infrastructure for digital matching services in Sweden. It is based on the idea of fewer silos and more common technical structures, solutions and standards. We want to go from ego to eco!\n\nAt Jobtech Development, we gather matching actors in an open network to collaborate and share data. Here you will find open data, standards and open source code that is freely available for everyone to use. Through our community there is the opportunity to discuss, ask questions, show cases and exchange ideas.\n\nJobTech Development is primarily aimed at people and companies that develop digital services for the labor market. We also collaborate with researchers and other actors who want in-depth insight into the labor market with the help of open data.\n<br><br><br><br>\n\n"
    menu:
        -
            title: 'Cooperate with us'
            url: /en/about-jobtech-development/samarbeta-med-oss
        -
            title: 'Get inspired by others'
            url: /en/about-jobtech-development/inspireras-av-andra
        -
            title: 'Contact us'
            url: /en/about-jobtech-development/kontakta-oss
routes:
    default: /about-jobtech-development
---

