---
title: 'Samarbeta med oss'
custom:
    content: "### Samarbeta med oss<BR>\n## Genom att vi samarbetar underlättar vi för arbetssökande och arbetsgivare att hitta varandra i digitala kanaler och bidrar därmed till en bättre arbetsmarknad. Det är logiken bakom JobTech Development. Samarbetet kan se ut på många olika sätt och ske på olika nivåer.\n\n###### Använd API:er och öppna data\nEtt sätt att samarbeta med JobTech Development är att använda de API:er, teknik och öppna data som finns tillgängliga via vår plattform. Vem som helst är välkommen att använda allt på plattformen helt kostnadsfritt.\n- Kolla in våra API:er\n- Kontakta oss om du behöver hjälp att komma igång\n    \n    \n###### Dela kunskap, erfarenheter, teknik eller data  \nEtt annat sätt att samarbeta med oss är att du och din organisation delar med er av era utmaningar, möjligheter och önskemål, så att vi får bättre förståelse för vilka behov som finns och kan möta dessa. Ni kan också via plattformen dela med er av teknik eller data till andra. Ingen ersättning utgår vid samarbeten.\n\n- Kontakta vår [Community manager](mailto:josefin.berndtson@arbetsformedlingen.se) så diskuterar vi vidare\n\n###### Återkoppla\nAtt ge oss feedback kring vad som fungerar bra/mindre bra är också ett utmärkt att samarbeta och ett viktigt bidrag till den fortsatta utvecklingen av JobTech Development. \n\n- Rapportera en bugg eller kom med ett förslag direkt till teamet via [Gitlab](https://gitlab.com/groups/arbetsformedlingen/-/issues)\n- Berätta för oss vad du saknar eller önskar i vårt [Community](https://forum.jobtechdev.se) eller [formulär](https://link.webropolsurveys.com/S/C1F32BE6730E1171).\n<br><br><br><br>"
    menu:
        -
            title: 'samarbeta med oss'
            url: /sv/om-jobtech-development/samarbeta-med-oss
        -
            title: 'Inspireras av andra'
            url: /sv/om-jobtech-development/inspireras-av-andra
        -
            title: 'kontakta oss'
            url: /sv/om-jobtech-development/kontakta-oss
---

