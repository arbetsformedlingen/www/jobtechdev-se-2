---
title: 'Kom igång och öppet forum'
custom:
    title: 'Kom igång och öppet forum'
    date: '27-08-2021 10:00'
    endtime: '11:30'
    description: 'Under hösten arrangerar JobTech Development öppna webbinarier för alla som vill ha hjälp att komma igång med våra API:er.'
    short: 'För användare av JobTech Developments API:er'
    register: 'https://jobtechdev.se/news/news_head/210316/'
---

