---
title: 'Onboarding and open forum'
custom:
    title: 'Onboarding and open forum'
    date: '19-05-2021 10:30'
    endtime: '11:00'
    description: 'During the autumn, JobTech Development will arrange open webinars for anyone who wants help getting started with our APIs.'
---

