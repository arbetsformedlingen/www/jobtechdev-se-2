---
title: 'eForvaltningsdagarna October 27-28 2021'
custom:
    title: eFörvaltningsdagarna
    date: '27-10-2021 09:00'
    short: 'JobTech Development will participate in the eFörvaltningsdagarna. e-Förvaltningsdagarna is Sweden''s largest conference and meeting place for e-government and digital transformation. More information to follow!'
    description: 'JobTech Development will participate in the eGovernment Days. eGovernment Days is Sweden''s largest conference and meeting place for e-government and digital transformation. More information to follow!'
    endtime: '18:00'
---

