---
title: 'Jobtech Atlas'
custom:
    title: 'Jobtech Atlas'
    description: 'A user interface that shows the data contained in the product Jobtech Taxonomy.'
    block_1: "Jobtech Atlas is a website for users to explore the database behind Jobtech Taxonomy. The database contains structures, concepts and relationships that are used to describe the labor market.  \nThere you can search among lists of concepts that often occur in matching in the labor market, such as competence concepts, job titles related to the SSYK structure (Standard for Swedish occupational classification) and search terms that can be used to facilitate search and filtering. The taxonomy database is managed by tArbetsförmedlingen and is based on information obtained from, among others, industry organizations, employers and jobseekers. The information is also available via the Jobtech Taxonomy API. \n\n## What problem does the product solve?\nAn important part of the work of producing and delivering data is to make it easily accessible. With Jobtech Atlas, the information contained in Jobtech Taxonomy is made available in a format that users have easier to read and understand than, for example, an API.\n\n## For whom is the product created?\nEveryone should be able to see and contribute to the language used in the labor market. This information has previously been difficult for non-developers or people outside Arbetsförmedlingen to access. To solve the problem and make the information available to everyone, we have built Jobtech Atlas."
    block_2: "### Notera\nProjektet är under utveckling och bör inte ses som produktionsklart. Funktioner kan komma att läggas till, tas bort eller modifieras. Designen är inte finjusterad och kommer sannolikt att ändras / förbättras.\nFör tillfället har vi fokuserat på att göra kompetenser och yrkesdata tillgängliga. Resterande data kommer snart också att finnas tillgängliga.\n\nVi välkomnar all eventuell feedback."
    contact_email: josefin.berndtson@arbetsformedlingen.se
    contact_name: 'Josefin Berndtson'
    product_info:
        -
            title: Version
            value: Beta
    menu:
        -
            title: 'User interface'
            url: 'https://atlas.jobtechdev.se/page/taxonomy.html'
            showInShort: '1'
        -
            title: 'Read me'
            url: 'https://gitlab.com/arbetsformedlingen/taxonomy-dev/frontend/jobtech-taxonomy-atlas/-/blob/master/README.md'
            showInShort: '1'
taxonomy:
    category:
        - Interface
    type:
        - 'Open data'
    status:
        - Beta
---

