---
title: JobSearch
custom:
    title: JobSearch
    block_1: "JobSearch is useful for all companies and organizations that want to use job ads from Platsbanken but do not have their own search engine.\n\nIt is also useful for players who have access to large amounts of structured data and want to be able to offer special search functions or niche advertising platforms."
    description: 'Search engine that makes it possible to search and filter among all job ads on the Arbetsförmedlingen advertising platform Platsbanken.'
    menu:
        -
            title: 'User interface'
            url: 'https://jobsearch.api.jobtechdev.se'
            showInShort: '1'
        -
            title: 'API key'
            url: 'https://apirequest.jobtechdev.se'
            showInShort: '1'
        -
            title: 'Getting started'
            url: 'https://github.com/Jobtechdev-content/Jobsearch-content/blob/master/GettingStartedJobSearchEN.md'
            showInShort: '1'
    block_2: "### Notice\nFor this API you need to register an API key. We may invalidate your key if you make large numbers of calls that do not fit the intended purpose of JobSearch."
    product_info:
        -
            title: Version
            value: '1.0'
    contact_email: josefin.berndtson@arbetsformedlingen.se
    contact_name: 'Josefin Berndtson'
taxonomy:
    category:
        - API
    type:
        - 'Open data'
---

