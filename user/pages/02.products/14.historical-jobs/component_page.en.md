---
title: 'Historical jobs'
custom:
    title: 'Historical jobs'
    description: 'All job ads that have been published by Arbetsförmedlingen since 2006.'
    contact_email: josefin.berndtson@arbetsformedlingen.se
    contact_name: 'Josefin Berndtson'
    menu:
        -
            title: Explore
            url: 'http://historik.azurewebsites.net'
            showInShort: '1'
        -
            title: 'Getting started'
            url: 'https://github.com/Jobtechdev-content/HistoricalJobs-content/blob/develop/GettingStartedHistoricalJobsSE.md'
            showInShort: '1'
    block_1: "Historical Jobs is a dataset with all job ads that have been published on Platsbanken since 2006. It also contains metadata (information about service, employer, place, time, etc.) and free text information. The dataset is continuously updated.\n \n## For whom is the product created?\nHistorical Jobs is useful for all companies and organizations that need large amounts of advertising data for statistics and analyzes in the labor market area. The large data base can also be used to build and train algorithms in connection with machine learning and for the development of new digital matching services."
    block_2: "Naming attributes for ad objects follows the naming used in JobStream. Recent years' ad objects contain more attributes than before. Values for attributes that do not exist are null.\n\n### Download\nAll: [2006-2020.rar](https://simonbe.blob.core.windows.net/afhistorik/pb_2006_2020.rar) / [2006-2020.zip](https://simonbe.blob.core.windows.net/afhistorik/pb_2006_2020.zip)\n\nYear by year: [2006.rar](https://minio.arbetsformedlingen.se/historiska-annonser/2006.rar), [2007.rar](https://minio.arbetsformedlingen.se/historiska-annonser/2007.rar), [2008.rar](https://minio.arbetsformedlingen.se/historiska-annonser/2008.rar), [2009.rar](https://minio.arbetsformedlingen.se/historiska-annonser/2009.rar), [2010.rar](https://minio.arbetsformedlingen.se/historiska-annonser/2010.rar), [2011.rar](https://minio.arbetsformedlingen.se/historiska-annonser/2011.rar), [2012.rar](https://minio.arbetsformedlingen.se/historiska-annonser/2012.rar), [2013.rar](https://minio.arbetsformedlingen.se/historiska-annonser/2013.rar), [2014.rar](https://minio.arbetsformedlingen.se/historiska-annonser/2014.rar), [2015.rar](https://minio.arbetsformedlingen.se/historiska-annonser/2015.rar), [2016.rar](https://minio.arbetsformedlingen.se/historiska-annonser/2016.rar), [2017.rar](https://minio.arbetsformedlingen.se/historiska-annonser/2017.rar), [2018.rar](https://minio.arbetsformedlingen.se/historiska-annonser/2018.rar), [2019.rar](https://minio.arbetsformedlingen.se/historiska-annonser/2019.rar), [2020.rar](https://minio.arbetsformedlingen.se/historiska-annonser/2020.rar), [2021.zip](https://minio.arbetsformedlingen.se/historiska-annonser/2021_first_6_months.zip)"
    product_info:
        -
            title: 'Data format'
            value: JSON
taxonomy:
    category:
        - Dataset
    type:
        - 'Open data'
---

