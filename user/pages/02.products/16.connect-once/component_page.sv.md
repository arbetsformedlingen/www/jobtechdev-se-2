---
title: 'Connect Once'
custom:
    title: 'Connect Once'
    description: 'Överför CV-data lagrade hos Arbetsförmedlingen till externa system med individens samtycke.'
    block_1: "Connect Once är en tjänst för överföring av data som skickar strukturerad data mellan två system. Tjänsten gör det möjligt för en person att automatiskt fylla i en arbetsansökan med hjälp av CV-uppgifterna som lagras hos Arbetsförmedlingen.\n\n## Vilket problem löser produkten?\nRekryterare och arbetsgivare upplever att jobbsökare avstår eller inte fullföljer ansökningar på grund av komplexitet och längd på ansökningsformulär. Connect Once förenkla jobbansökningen genom att jobbsökare kan använda sin CV-data som lagras hos Arbetsförmedlingen för att fylla i sitt formulär med ett klick. Genom att förkorta den tid som krävs för att ansöka om ett jobb kommer volymen av fullföljda ansökningar till arbetsgivaren öka samtidigt som jobbsökarens upplevelse av processen förbättras.\n\n## För vem är produkten skapad?\nConnect Once är avsett för rekryterare och arbetsgivare som vill öka andelen fullföljda ansökningar. Produkten kan också användas för att skicka annan strukturerad data mellan två system som t.ex. registreringsformulär för onlinetjänster."
    block_2: "### Connect Once API\nAPI:et betatestas just nu och kommer bli tillgängligt för alla inom kort. Kontakta oss för mer information eller om ni önskar att testa tjänsten. "
    product_info:
        -
            title: Licens
            value: 'Apache License 2.0'
        -
            title: Version
            value: 1.1.0-beta
    contact_email: josefin.berndtson@arbetsformedlingen.se
    contact_name: 'Josefin Berntson'
    menu:
        -
            title: Demo
            url: 'https://github.com/MagnumOpuses/af-connect-demo/blob/master/README.md'
            showInShort: '1'
        -
            title: 'Kom igång'
            url: 'https://github.com/MagnumOpuses/af-connect-module'
            showInShort: '1'
taxonomy:
    category:
        - API
    type:
        - 'Öppna data'
        - 'Öppen källkod'
    status:
        - Beta
---

