---
title: 'JobAd Enrichments'
custom:
    description: 'AI-lösning som identifierar och extraherar relevant information i ostrukturerade jobbannonser.'
    contact_name: 'Josefin Berntson'
    version: '1.0'
    format: JSON
    licens: MIT
    publishdate: '19-05-2021 10:00'
    block_1: "JobAd Enrichments är en AI-lösning som automatiskt hämtar ut relevanta ord och uttryck i platsannonser samtidigt som överflödig information filtreras bort. API:et bidrar till en mer träffsäker matchning mellan arbetsgivare och arbetssökande, och gör det lättare att navigera och snabbt hitta rätt på digitala annonsplattformar.\n\n## Vilket problem löser produkten?\nIrreleventa sökträffar är ett vanligt problem bland användare av digitala matchningstjänster och annonsplattformar. Att tvingas lägga tid på att rensa och sortera bland sökträffar försvårar jobbsökandet. Med JobAd Enrichments kan problemen minimeras, och aktörer som erbjuder digitala tjänster slipper lägga tid och resurser på manuell hantering.\n\n## För vem är produkten skapad? \nJobAd Enrichments är användbart för alla företag och organisationer som erbjuder en digital matchningstjänst eller annonsplattform, och som önskar förbättra den. API:et kan också användas för utveckling av nya innovativa digitala tjänster eller för att få fördjupad insikt om arbetsmarknaden."
    block_2: "**Implementation och användning**  \nVi rekommenderar att du kontaktar oss innan du implementerar JobAd Enrichments direkt mot ditt system så att vi kan ge råd och vägledning runt API:ets möjligheter och begränsningar."
    contact_email: josefin.berntdson@arbetsformedlingen.se
    title: 'JobAd Enrichments'
    product_info:
        -
            title: Version
            value: '1.0'
    menu:
        -
            title: Användargränssnitt
            url: 'https://jobad-enrichments-api.jobtechdev.se'
            showInShort: '1'
        -
            title: API-nyckel
            url: 'https://apirequest.jobtechdev.se'
            showInShort: '1'
        -
            title: 'Kom igång'
            url: 'https://github.com/Jobtechdev-content/JobAdEnrichments-content/blob/master/GettingstartedJobAdEnrichmentsSE.md'
            showInShort: '1'
taxonomy:
    category:
        - API
    type:
        - 'Öppna data'
---

