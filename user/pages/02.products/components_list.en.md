---
title: Products
custom:
    showLeftMenu: false
    components:
        -
            page: /products/alljobads
        -
            page: /products/connect-once
        -
            page: /products/ekosystem_foer_annonser
        -
            page: /products/etik-och-digital-matchning
        -
            page: /products/giglab-sverige
        -
            page: /products/historical-jobs
        -
            page: /products/jobad-enrichments
        -
            page: /products/jobsearch
        -
            page: /products/jobstream
        -
            page: /products/jobtech-atlas
        -
            page: /products/jobtech-taxonomy
        -
            page: /products/yrkesprognoser
        -
            page: /products/open-plattforms
    text: '##### Here we present the open data and technical solutions that are freely available for anyone to use and create benefits for more.'
---

