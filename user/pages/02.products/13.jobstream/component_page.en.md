---
title: JobStream
custom:
    title: JobStream
    description: 'Real-time data from all job ads published by Arbetsförmedlingen.'
    block_1: "JobStream provides access to all job ads published in Platsbanken, including real-time information about changes that take place around or in these advertisements, e.g. publications, unpublishings or updates of ad texts.\n \n## For whom is the product created?\nJobStream is useful for all companies and organizations that have their own search engine and want to improve their service with real-time updated ad data. The API can also be used to build and train algorithms in connection with machine learning or to identify and analyze trends in the labor market based on advertising data."
    menu:
        -
            title: 'User interface'
            url: 'https://jobstream.api.jobtechdev.se'
            showInShort: '1'
        -
            title: 'API key'
            url: 'https://apirequest.jobtechdev.se'
            showInShort: '1'
        -
            title: 'Getting started'
            url: 'https://github.com/Jobtechdev-content/Jobstream-content/blob/develop/GettingStartedJobStreamSE.md'
            showInShort: '1'
        -
            title: 'Code examples'
            url: 'https://github.com/JobtechSwe/getting-started-code-examples'
            showInShort: null
    product_info:
        -
            title: Version
            value: 1.17.1
        -
            title: 'Data format'
            value: JSON
    contact_email: josefin.berntson@arbetsformedlingen.se
    contact_name: 'Josefin Berndtson'
taxonomy:
    category:
        - API
    type:
        - 'Open data'
---

