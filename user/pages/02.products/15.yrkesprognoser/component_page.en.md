---
title: 'Occupational forecasts'
custom:
    title: 'Occupational forecasts'
    description: 'Forecasts for recruitment needs and competitive situation in different professions.'
    block_1: "Occupational forecasts make it possible to show forecasts of the need for recruitment in different professions and occupational groups in one and five years' time, respectively. The forecasts are based on interviews with employers, which are conducted twice a year by SCB and Arbetsförmedlingen. The API also provides information on the competitive situation a jobseeker can expect in different professions.\n \n## For whom is the product created?\nThe API is useful for all companies and organizations that offer services in vocational and study guidance and who want to help their target groups make informed decisions before career and study choices. Actors who want in-depth insights into developments in the labor market also benefit from the API.\n"
    block_2: "### Notice!\nOccupational forecasts API is discontinued, no new forecasts will be published through this API. Development work is underway to produce occupational forecasts for 2021. The publication date for new assessments has not yet been set. Occupational forecasts for the year 2021 will be made available as a downloadable file (download link is published here).\n\nCOVID 19: Covid-19 has changed the labor market situation for some professions. The forecasts to be interpreted with caution have been highlighted in the file [Bedömningar att flagga](https://www.jobtechdev.se/user/pages/files/covid.csv)"
    menu:
        -
            title: 'User interface'
            url: 'https://api.arbetsformedlingen.se/af/v2/forecasts/api/#!/forecasts/'
            showInShort: '1'
        -
            title: 'Getting started'
            url: 'https://github.com/Jobtechdev-content/Yrkesprognoser-content/blob/develop/GettingStartedOccupationForecastSE.md'
            showInShort: '1'
    product_info:
        -
            title: Version
            value: 1.0.84
        -
            title: License
            value: CC
    contact_email: josefin.berndtson@arbetsformedlingen.se
    contact_name: 'Josefin Berndtson'
taxonomy:
    category:
        - API
    type:
        - 'Open data'
---

