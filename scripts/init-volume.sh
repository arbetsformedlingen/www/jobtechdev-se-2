#!/bin/bash

if [ -d /mnt/user ]
then
    echo Volume already initiate, will ensure theme images is up to date.
    cp -Ra /var/www/html-base/user/data/images /mnt/user/data/
else
    echo Initialize volume, create initial setup and site.
    mkdir -p /mnt/user/pages /mnt/user/data \
            /mnt/user/accounts /mnt/sessions /mnt/logs
    cp -Ra /var/www/html-base/user /mnt/
fi
